import { User } from './user.model'; // Adjust the path based on your project structure
import { Tweet } from './tweet.model'; // Adjust the path based on your project structure
export class Like {
  id?: number;
  userId?: number;
  tweetId?: number;
  user?: Partial<User>;
  tweet?: Partial<Tweet>;
}
