export interface User {
  id: number;
  username: string;
  password: string;
  email: string;
  displayName: string;
  bio: string;
  createdAt: Date;
  updatedAt: Date;
}
