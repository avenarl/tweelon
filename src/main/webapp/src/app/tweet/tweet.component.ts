import { Component, OnInit } from '@angular/core';
import { Tweet } from '../models/tweet.model';
import { TweetService } from '../services/tweet.service';
import { User } from '../models/user.model';
import { AuthService } from '../services/auth.service';
import { RetweetService } from '../services/retweet.service';

@Component({
  selector: 'app-tweet',
  templateUrl: './tweet.component.html',
  styleUrls: ['./tweet.component.css'],
})
export class TweetComponent implements OnInit {
  tweets: Tweet[] = [];
  newTweet: Tweet = new Tweet();
  currentUser: User | null = null;

  constructor(
    private tweetService: TweetService,
    private authService: AuthService,
    private retweetService: RetweetService
  ) {}

  ngOnInit(): void {
    this.getTweets();
    this.currentUser = this.authService.currentUserValue;
  }

  getTweets(): void {
    this.tweetService.getTweets().subscribe((tweets: Tweet[]) => {
      this.tweets = tweets;
      console.log(tweets);
    });
  }

  createTweet(): void {
    const currentUser = this.authService.currentUserValue;
    if (currentUser && currentUser.id) {
      this.tweetService
        .createTweet(currentUser.id, this.newTweet)
        .subscribe(() => {
          this.newTweet = new Tweet();
          this.getTweets();
        });
    } else {
      console.error('No user is currently logged in.');
    }
  }

  retweet(tweet: number): void {
    const currentUser = this.authService.currentUserValue;
    if (currentUser && currentUser.id) {
      this.retweetService.createRetweet(currentUser.id, tweet).subscribe(() => {
        console.log('Retweet successful');
      });
    } else {
      console.error('No user is currently logged in.');
    }
  }
}
