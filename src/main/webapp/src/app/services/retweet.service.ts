import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Retweet } from '../models/retweet.model';

@Injectable({
  providedIn: 'root',
})
export class RetweetService {
  private apiServerUrl = 'http://localhost:8080/api/v1/retweet';

  constructor(private http: HttpClient) {}

  createRetweet(userId: number, tweetId: number): Observable<Retweet> {
    return this.http.post<Retweet>(
      `${this.apiServerUrl}/${userId}/${tweetId}`,
      {}
    );
  }

  deleteRetweet(retweetId: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServerUrl}/${retweetId}`);
  }

  getAllRetweets(): Observable<Retweet[]> {
    return this.http.get<Retweet[]>(`${this.apiServerUrl}`);
  }

  getRetweetsByUserId(userId: number): Observable<Retweet[]> {
    return this.http.get<Retweet[]>(`${this.apiServerUrl}/${userId}`);
  }

  getRetweetsByTweetId(tweetId: number): Observable<Retweet[]> {
    return this.http.get<Retweet[]>(`${this.apiServerUrl}/${tweetId}`);
  }
}
