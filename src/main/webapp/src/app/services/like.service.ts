import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Like } from '../models/like.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class LikeService {
  private apiServerUrl = 'http://localhost:8080/api/v1/like';
  constructor(private http: HttpClient) {}

  createLike(like: Like): Observable<Like> {
    return this.http.post<Like>(`${this.apiServerUrl}/`, like);
  }

  deleteLike(id: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServerUrl}/${id}`);
  }
}
